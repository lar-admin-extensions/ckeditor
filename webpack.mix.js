const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'resources/assets/js')
	.sass('resources/js/app.scss', 'resources/assets/css');