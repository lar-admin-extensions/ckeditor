<?php

return [
    "name" => "ckeditor",
    "group" => "ckeditor",
    "path" => "lar-admin-extensions/ckeditor",
    "description" => "",
    "version" => "1.0.0",
    "icon" => "nut",
    "namespace" => "Lar\\Admin\\Ckeditor",
    "class" => "Ckeditor",
    "repo" => "admin-ext:lar-admin-extensions/ckeditor.git"
];