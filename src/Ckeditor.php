<?php

namespace Lar\Admin\Ckeditor;

use Lar\Admin\Commands\Update;
use Lar\Layout\Commands\HeaderDumpUpdate;
use Lar\Admin\Commands\ExtendInstall;
use Lar\Admin\Commands\ExtendRemove;

/**
 * Ckeditor Class
 * 
 * @package Lar\Admin\Ckeditor
 */
class Ckeditor
{
    /**
     * Static method update
     * 
     * @return void
     */
    static function update(Update $update) {
        
        $update->info('Extension [Ckeditor] updated!');
    }

    /**
     * Static method dump
     * 
     * @return void
     */
    static function dump(HeaderDumpUpdate $dump) {
        
        $dump->info('Extension [Ckeditor] dumped!');
    }

    /**
     * Static method install
     * 
     * @return void
     */
    static function install(ExtendInstall $installer) {
        
        $installer->info('Extension [Ckeditor] installed!');
    }

    /**
     * Static method uninstall
     * 
     * @return void
     */
    static function uninstall(ExtendRemove $uninstaller) {
        
        $uninstaller->info('Extension [Ckeditor] run uninstall...');
    }

}
