<?php

namespace Lar\Admin\Ckeditor;

use Illuminate\Support\Facades\Facade;

/**
 * CkeditorFacade Class
 * 
 * @package Lar\Admin\Ckeditor
 */
class CkeditorFacade extends Facade
{
    /**
     * Protected Static method getFacadeAccessor
     * 
     * @return void
     */
    protected static function getFacadeAccessor() {
        return Ckeditor::class;
    }

}
